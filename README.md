## Explain your choices regarding stack used as well as alternatives and their pros and cons?

I decided to choose Angular framework with his latest version. This is my core framework in JS in which I specializes. I feel good and work smoothly with him. Regarding UX framework, I decided to use also framework which I'm working daily at my current job and which I am using in my private project as it is beautiful, has lots of ready components and has really nice documentation which is very important. It is Angular Material.

## How would you go with application internationalisation?

I'm currently working with ngx-translate -> the internationalization (i18n) library for Angular, I think one of the most popular. I know how it works, it is quite easy to configure it for a project so I think I will go this way.

## What could impact this application's performance most in longer term and what would be your ways to optimize it?

I think the most problematic in this type of app and exactly this app is pinging external API. We could cache responses with parameters. That will be the best way to optimize the app.

## What if you had to make it work on lower-end devices and slow internet connection? What trade-offs would have to be made?

I think one of the most common and famous is PWA now. So I think I would add a service worker to this project. On the other side if I would like to avoid PWA We could cache api response for some period and store it in repositories or add ngrx for that. Lazy loading is already in the app but there is only one module so there is nothing to do more. The same is with OnPushStrategy which I already added in comment component.

## How would you report and collect errors from application?

There are lots of tools which can help to do it, but I worked and working with Sentry which is really helpful and with raven-js npm package for communication.

## How long did it take you to code this app?

I started this app on 5th of November. It took me four days to finish all goals and add some UX. I worked a few hours a day, but it is hard to count exactly how many hours it took for me. On average I spent maybe 4-5 hours a day.

