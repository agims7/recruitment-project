import { RedditData } from './reddit-data.model';

export class RedditResponse {

    constructor(
      public data: RedditData,
      public kind: string = ''
    ) { }
  }
