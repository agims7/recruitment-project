import { Reddit } from './reddit.model';

export class RedditContent {

  constructor(
    public data: Reddit,
    public kind: string = '',
  ) { }
}
