import { RedditContent } from './reddit-content.model';

export class RedditData {

  constructor(
    public after: string = '',
    public before: string = '',
    public children: RedditContent[]
  ) { }
}
