export class Reddit {

  constructor(
    public id: string = '',
    public archived: boolean = false,
    public author: string = '',
    public kind: string = '',
    public created_utc: number = 0,
    public hidden: boolean = false,
    public name: string = '',
    public num_comments: number = 0,
    public score: number = 0,
    public selftext: string = '',
    public subreddit: string = '',
    public subreddit_id: string = '',
    public thumbnail: string = '',
    public title: string = '',
    public url: string = '',
  ) { }
}
