export class Comment {

  constructor(
    public id: string = '',
    public author: string = '',
    public body: string = '',
    public created_utc: number = 0,
    public kind: string = '',
    public replies: Comment[],
  ) { }
}
