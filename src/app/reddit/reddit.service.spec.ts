import { TestBed, inject } from '@angular/core/testing';

import { RedditService } from './reddit.service';

describe('Service: RedditService', () => {

  let redditService: RedditService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        RedditService,
      ]
    })
  });

  beforeEach(
    inject([RedditService], (service: RedditService) => {
      redditService = service;
    })
  );

  it('should be created', () => {
    expect(redditService).toBeTruthy();
  });
});
