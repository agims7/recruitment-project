import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

import { CategoriesComponent } from './categories.component';
import { RedditService } from '@app/reddit/reddit.service';
import { UiScrollerModule } from '@app/ui/ui-scroller.module';

const redditServiceMock = {
  selectedCategory$: new BehaviorSubject<string>('')
};

describe('Component: CategoriesComponent', () => {

  let component: CategoriesComponent;
  let fixture: ComponentFixture<CategoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        ReactiveFormsModule,
        UiScrollerModule,

        // Material
        MatFormFieldModule,
        MatInputModule,
      ],
      declarations: [
        CategoriesComponent
      ],
      providers: [
        {
          provide: RedditService,
          useValue: redditServiceMock
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoriesComponent);
    component = fixture.componentInstance;
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

});
