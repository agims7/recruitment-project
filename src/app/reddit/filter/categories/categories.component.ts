import { Component, OnInit } from '@angular/core';

import { RedditService } from '@app/reddit/reddit.service';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

@Component({
  selector: 'smp-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {

  public availableCategories: string[] = [
    'pics',
    'videos',
    'gaming',
    'movies',
    'funny',
    'history',
  ];
  public selectedCategory: string = null;

  private componentSubscription: Subscription = new Subscription();

  constructor(
    public redditService: RedditService
  ) { }

  ngOnInit(): void {
    const categoryWatch: Subscription = this.watchCategoryChange();

    this.componentSubscription.add(categoryWatch);
  }


  public changeCategory(category): void {
    if (this.selectedCategory === category) {
      category = null;
    }

    this.redditService.selectedCategory$.next(category);
    this.selectedCategory = category;
  }

  private watchCategoryChange(): Subscription {
    return this.redditService.selectedCategory$
      .pipe(
        take(1)
      )
      .subscribe(
        (category: string) => {
          this.selectedCategory = category;
        }
      );
  }

}
