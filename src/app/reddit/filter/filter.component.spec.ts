
import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';

import { CategoriesComponent } from './categories/categories.component';
import { FilterComponent } from './filter.component';
import { ListComponent } from '../list/list.component';
import { RedditService } from '../reddit.service';
import { SearchComponent } from './search/search.component';
import { SortComponent } from './sort/sort.component';
import { UiScrollerModule } from '@app/ui/ui-scroller.module';

describe('Component: FilterComponent', () => {

  let component: FilterComponent;
  let fixture: ComponentFixture<FilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
        UiScrollerModule,

        // Material
        MatCardModule,
        MatFormFieldModule,
        MatInputModule,
        MatProgressSpinnerModule,
        MatRadioModule,
      ],
      declarations: [
        CategoriesComponent,
        FilterComponent,
        ListComponent,
        SearchComponent,
        SortComponent,
      ],
      providers: [
        RedditService
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterComponent);
    component = fixture.componentInstance;
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

});
