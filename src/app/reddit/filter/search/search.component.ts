import { Component, OnInit, OnDestroy } from '@angular/core';
import { Validators, FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { debounceTime, filter, take } from 'rxjs/operators';

import { RedditService } from '@app/reddit/reddit.service';

@Component({
  selector: 'smp-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, OnDestroy {

  public search: FormControl = new FormControl(
    '',
    [
      Validators.maxLength(512)
    ]
  );

  private componentSubscription: Subscription = new Subscription();

  constructor(
    private redditService: RedditService,
  ) { }

  ngOnInit(): void {
    const queryWatch: Subscription = this.watchQueryChange();
    const queryStreamWatch: Subscription = this.watchQueryStreamChange();

    this.componentSubscription.add(queryWatch);
    this.componentSubscription.add(queryStreamWatch);
  }

  ngOnDestroy(): void {
    this.componentSubscription.unsubscribe();
  }

  private watchQueryChange(): Subscription {
    return this.search.valueChanges
      .pipe(
        filter(
          (query: string) => query.length > 2
        ),
        debounceTime(500),
      )
      .subscribe(
        (query: string) => {
          this.redditService.queryChange$.next(query);
        }
      );
  }

  private watchQueryStreamChange(): Subscription {
    return this.redditService.queryChange$
      .pipe(
        take(1)
      )
      .subscribe(
        (query: string) => {
          this.search.setValue(query);
        }
      );
  }


}
