import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

import { RedditService } from '@app/reddit/reddit.service';
import { SearchComponent } from './search.component';
import { UiScrollerModule } from '@app/ui/ui-scroller.module';

const redditServiceMock = {
  queryChange$: new BehaviorSubject<string>('')
};

describe('Component: SearchComponent', () => {

  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        ReactiveFormsModule,
        UiScrollerModule,

        // Material
        MatFormFieldModule,
        MatInputModule,
      ],
      declarations: [
        SearchComponent
      ],
      providers: [
        {
          provide: RedditService,
          useValue: redditServiceMock
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

});
