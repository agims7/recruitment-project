import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';

import { RedditService } from '@app/reddit/reddit.service';
import { SortComponent } from './sort.component';
import { UiScrollerModule } from '@app/ui/ui-scroller.module';

const redditServiceMock = {
  sortChange$: new BehaviorSubject<string>('')
};

describe('Component: SortComponent', () => {

  let component: SortComponent;
  let fixture: ComponentFixture<SortComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        FormsModule,
        UiScrollerModule,

        // Material
        MatFormFieldModule,
        MatInputModule,
        MatRadioModule
      ],
      declarations: [
        SortComponent
      ],
      providers: [
        {
          provide: RedditService,
          useValue: redditServiceMock
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SortComponent);
    component = fixture.componentInstance;
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

});
