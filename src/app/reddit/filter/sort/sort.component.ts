import { Component } from '@angular/core';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

import { RedditService } from '@app/reddit/reddit.service';
import { SORT_TYPE } from '@app/shared/enums/sort-type.enum';

@Component({
  selector: 'smp-sort',
  templateUrl: './sort.component.html',
  styleUrls: ['./sort.component.scss']
})
export class SortComponent {

  public sortType: string = null;

  private componentSubscription: Subscription = new Subscription();

  constructor(
    private redditService: RedditService,
  ) { }

  ngOnInit(): void {
    const sortWatch: Subscription = this.watchSortChange();

    this.componentSubscription.add(sortWatch);
  }

  public typeChanged(): void {
    this.redditService.sortChange$.next(this.sortType);
  }

  private watchSortChange(): Subscription {
    return this.redditService.sortChange$
      .pipe(
        take(1)
      )
      .subscribe(
        (sort: string) => {
          this.sortType = sort;
        }
      );
  }

}
