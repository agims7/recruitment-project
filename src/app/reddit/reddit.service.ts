import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { ACTIVE_MODE } from '@app/shared/enums/active-mode.enum';
import { RedditContent } from '@app/shared/models/reddit-content.model';
import { SORT_TYPE } from '@app/shared/enums/sort-type.enum';

@Injectable()
export class RedditService {

  public activeMode: ACTIVE_MODE = null;
  public activeReddit: RedditContent = null;
  public queryChange$: BehaviorSubject<string> = new BehaviorSubject<string>('');
  public selectedCategory$: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  public SORT_TYPE = SORT_TYPE;
  public sortChange$: BehaviorSubject<string> = new BehaviorSubject<string>(SORT_TYPE.NEW);

  constructor() { }
}
