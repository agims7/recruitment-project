import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Comment } from '@app/shared/models/comment.model';
import { environment } from '@env/environment';
import { Reddit } from '@app/shared/models/reddit.model';
import { RedditContent } from '@app/shared/models/reddit-content.model';
import { RedditData } from '@app/shared/models/reddit-data.model';
import { RedditResponse } from '@app/shared/models/reddit-response.model.';
import { RedditService } from './reddit.service';

@Injectable()
export class RedditRepository {

  public lastElementOnList: string = null;

  constructor(
    private http: HttpClient,
    private redditService: RedditService,
  ) { }

  public getList(sort: string = null, category: string = null, query: string = null): Observable<RedditContent[]> {
    const options = {};
    let urlFirstPath = null;
    let urlSecondPath = '/new.json';

    if (this.lastElementOnList) {
      Object.assign(options, { params: new HttpParams().set('limit', '25').append('after', this.lastElementOnList) });
    }

    if (sort === 'hot') {
      urlSecondPath = '/hot.json';
    }

    if (query) {
      urlSecondPath = '/search.json';

      Object.assign(options, { params: new HttpParams().set('q', query) });
    }

    if (category) {
      urlFirstPath = `/r/${category}`;
    }

    const url: string = urlFirstPath ? urlFirstPath + urlSecondPath : urlSecondPath;

    return this.http.get(
      environment.redditUrl + url,
      options
    )
      .pipe(
        map((response: any): RedditContent[] => {
          const redditResponse = this.mapToRedditResponse(response);

          this.lastElementOnList = redditResponse.data.after;

          return redditResponse.data.children;
        })
      );
  }

  public getComments(subreddit: string, id: string): Observable<Comment[]> {
    const options = {
      params: new HttpParams().set('depth', '25')
    };

    return this.http.get(
      environment.redditUrl + '/r/' + subreddit + '/comments/' + id + '.json',
      options
    )
      .pipe(
        map((reddits: any): Comment[] => {
          const commentList: Comment[] = (reddits[1].data.children || []).map(
            (data: any): Comment => this.mapToComment(data)
          );

          return commentList;
        }
        )
      );
  }

  private mapToRedditResponse(data: any): RedditResponse {
    if (
      null === data ||
      undefined === data
    ) {
      return null;
    }

    return new RedditResponse(
      this.mapToRedditData(data.data),
      data.kind || '',
    );
  }

  private mapToRedditData(data: any): RedditData {
    if (
      null === data ||
      undefined === data
    ) {
      return null;
    }

    const children: RedditContent[] = (data.children || []).map(
      (child: any): RedditContent => this.mapToRedditContent(child)
    );

    return new RedditData(
      data.after || '',
      data.before || '',
      children
    );
  }

  private mapToRedditContent(data: any): RedditContent {
    if (
      null === data ||
      undefined === data
    ) {
      return null;
    }

    return new RedditContent(
      this.mapToReddit(data.data),
      data.kind || '',
    );
  }

  private mapToReddit(data: any): Reddit {
    if (
      null === data ||
      undefined === data
    ) {
      return null;
    }

    return new Reddit(
      data.id || '',
      !!data.archived,
      data.author || '',
      data.kind || '',
      data.created_utc || 0,
      !!data.hidden,
      data.name || '',
      data.num_comments || 0,
      data.score || 0,
      data.selftext || '',
      data.subreddit || '',
      data.subreddit_id || '',
      data.thumbnail || '',
      data.title || '',
      data.url || '',
    );
  }

  private mapToComment(data: any): Comment {
    if (
      null === data ||
      undefined === data
    ) {
      return null;
    }

    const repliesChildren = data.data.replies ? data.data.replies.data.children : null;
    const replies: Comment[] = (repliesChildren || []).map(
      (data: any): Comment => this.mapToComment(data)
    );

    return new Comment(
      data.data.id || '',
      data.data.author || '',
      data.data.body || '',
      data.data.created_utc || 0,
      data.kind || '',
      replies
    );
  }
}
