import { TestBed, inject } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { DetailsGuard } from './details.guard';
import { Reddit } from '@app/shared/models/reddit.model';
import { RedditContent } from '@app/shared/models/reddit-content.model';
import { RedditService } from './reddit.service';
import { Router } from '@angular/router';

const routerMock = {
  navigate: jest.fn()
};

const redditServiceMock = {};

describe('Guard: DetailsGuard', () => {

  let detailsGuard: DetailsGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      providers: [
        DetailsGuard,
        {
          provide: RedditService,
          useValue: redditServiceMock
        },
        {
          provide: Router,
          useValue: routerMock
        },
      ]
    })
  });

  beforeEach(
    inject([DetailsGuard], (guard: DetailsGuard) => {
      detailsGuard = guard;
    })
  );

  it('should be created', () => {
    expect(detailsGuard).toBeTruthy();
  });

  it('should return true if there is active reddit selected in reddit service', () => {
    detailsGuard["redditService"] = {...detailsGuard["redditService"], activeReddit: new RedditContent(new Reddit)};

    expect(detailsGuard.canActivate()).toEqual(true);
  });

  it('should return false if there is not active reddit selected in reddit service', () => {
    detailsGuard["redditService"] = {...detailsGuard["redditService"], activeReddit: null};

    expect(detailsGuard.canActivate()).toEqual(false);
  });

});
