import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

import { RedditService } from './reddit.service';

@Injectable()
export class DetailsGuard implements CanActivate {

  constructor(
    private redditService: RedditService,
    private router: Router
  ) {

  }

  canActivate(): boolean {
    if (!!this.redditService.activeReddit) {
      return true;
    }

    this.navigateTo('reddit/list');

    return false;
  }

  private navigateTo(path: string): void {
    this.router.navigate([path], { replaceUrl: true });
  }

}
