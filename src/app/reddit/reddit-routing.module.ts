import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CommentsResolve } from './details/comment/comments.resolve';
import { DetailsComponent } from './details/details.component';
import { DetailsGuard } from './details.guard';
import { ListComponent } from './list/list.component';
import { ListResolve } from './list/list.resolve';
import { RedditComponent } from './reddit.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        redirectTo: 'reddit'
      },
      {
        path: 'reddit',
        component: RedditComponent,
        children: [
          {
            path: '',
            redirectTo: 'list'
          },
          {
            path: 'list',
            component: ListComponent,
            resolve: {
              reddits: ListResolve
            }
          },
          {
            path: 'details/:id',
            canActivate: [DetailsGuard],
            component: DetailsComponent,
            resolve: {
              comments: CommentsResolve
            }
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ],
  providers: [
    CommentsResolve,
    DetailsGuard,
    ListResolve
  ]
})
export class RedditRoutingModule { }
