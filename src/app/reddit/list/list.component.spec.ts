import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Router, ActivatedRoute } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ChangeDetectorRef } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';

import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatToolbarModule } from '@angular/material/toolbar';

import { CategoriesComponent } from '../filter/categories/categories.component';
import { FilterComponent } from '../filter/filter.component';
import { ListComponent } from './list.component';
import { RedditRepository } from '../reddit.repository';
import { RedditService } from '../reddit.service';
import { SearchComponent } from '../filter/search/search.component';
import { SortComponent } from '../filter/sort/sort.component';
import { UiScrollerModule } from '@app/ui/ui-scroller.module';
import { RedditContent } from '@app/shared/models/reddit-content.model';
import { Reddit } from '@app/shared/models/reddit.model';

const routerMock = {
  navigate: jest.fn()
};

const redditServiceMock = {
  queryChange$: new BehaviorSubject<string>(''),
  selectedCategory$: new BehaviorSubject<string>(''),
  sortChange$: new BehaviorSubject<string>(''),
  activeReddit: null
};

const activatedRouteMock = {
  snapshot: {
    data: {
      reddits: null
    }
  }
};

describe('Component: ListComponent', () => {
  let component: ListComponent;
  let fixture: ComponentFixture<ListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        FormsModule,
        HttpClientModule,
        ReactiveFormsModule,
        RouterTestingModule,
        UiScrollerModule,

        // Material
        MatButtonModule,
        MatCardModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatProgressSpinnerModule,
        MatRadioModule,
        MatToolbarModule,
      ],
      declarations: [
        CategoriesComponent,
        FilterComponent,
        ListComponent,
        SearchComponent,
        SortComponent,
      ],
      providers: [
        ChangeDetectorRef,
        RedditRepository,
        {
          provide: RedditService,
          useValue: redditServiceMock
        },
        {
          provide: Router,
          useValue: routerMock
        },
        {
          provide: ActivatedRoute,
          useValue: activatedRouteMock
        },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListComponent);
    component = fixture.componentInstance;
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it('should call getMoreListData on reachYEnd event if isLoading is false and change isLoading state to true', () => {
    component.onReachYEnd();

    expect(component.isLoading).toBeTruthy();
  });

  it('should return true if string container has  http or https', () => {
    const result = component.hasThumbnail('https://www.google.com/');

    expect(result).toBeTruthy();
  });

  it('should return false if string container has not http or https', () => {
    const result = component.hasThumbnail('www.google.com');

    expect(result).toBeFalsy();
  });

  it('should return 5h ago string', () => {
    const date = new Date();
    const ago = (date.getTime() - (60*60*5*1000))/1000;
    const result = component.timeFrom(ago);

    expect(result).toBe('5 hours ago');
  });

});
