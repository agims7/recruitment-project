import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { RedditContent } from '@app/shared/models/reddit-content.model';
import { RedditRepository } from '../reddit.repository';

@Injectable()
export class ListResolve implements Resolve<RedditContent[]> {

  constructor(
    private redditRepository: RedditRepository
  ) { }

  resolve(): Observable<RedditContent[]> {
    return this.redditRepository.getList()
    .pipe(
      catchError(
        () => of(null)
      )
    );
  }

}
