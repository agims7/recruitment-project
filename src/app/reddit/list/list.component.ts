import { Component, OnInit, OnDestroy, ViewChild, ChangeDetectorRef, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { distinctUntilChanged, finalize } from 'rxjs/operators';
import * as _moment from 'moment';

import { ACTIVE_MODE } from '@app/shared/enums/active-mode.enum';
import { RedditContent } from '@app/shared/models/reddit-content.model';
import { RedditRepository } from '../reddit.repository';
import { RedditService } from '../reddit.service';

const moment = _moment;

@Component({
  selector: 'smp-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit, OnDestroy {

  public ACTIVE_MODE = ACTIVE_MODE;
  public reddits: RedditContent[] = [];
  public isLoading: boolean = false;

  private activeCategory: string = null;
  private activeQuery: string = null;
  private activeSort: string = null;
  private componentSubscription: Subscription = new Subscription();

  constructor(
    private activatedRoute: ActivatedRoute,
    private cd: ChangeDetectorRef,
    private redditRepository: RedditRepository,
    private redditService: RedditService,
    private router: Router,
    private ngZone: NgZone,
  ) {
    this.fetchData();
    this.redditService.activeMode = ACTIVE_MODE.LIST;
  }

  ngOnInit(): void {
    const categoryWatch: Subscription = this.watchCategoryChange();
    const queryWatch: Subscription = this.watchQueryChange();
    const sortWatch: Subscription = this.watchSortChange();

    this.componentSubscription.add(categoryWatch);
    this.componentSubscription.add(queryWatch);
    this.componentSubscription.add(sortWatch);
  }

  ngOnDestroy(): void {
    this.componentSubscription.unsubscribe();
  }

  public selectReddit(reddit: RedditContent): void {
    this.redditService.activeReddit = reddit;
    this.redditRepository.lastElementOnList = null;

    this.ngZone.run(() => {
      this.router.navigate(['reddit', 'details', reddit.data.id]);
    });
  }

  public onReachYEnd(): void {
    if (!this.isLoading) {
      this.getMoreListData();
    }
  }

  public hasThumbnail(url: string): boolean {
    return url.includes("https") || url.includes("http");
  }

  public trackByFn(index, item: RedditContent): number {
    return index;
  }

  public timeFrom(time: number): string {
    return moment(time * 1000).fromNow();
  }

  private watchCategoryChange(): Subscription {
    return this.redditService.selectedCategory$
      .pipe(
        distinctUntilChanged()
      )
      .subscribe(
        (category: string) => {
          this.activeCategory = category;
          this.getListData()
        }
      );
  }

  private watchQueryChange(): Subscription {
    return this.redditService.queryChange$
      .pipe(
        distinctUntilChanged()
      )
      .subscribe(
        (query: string) => {
          this.activeQuery = query;
          this.getListData();
        }
      );
  }

  private watchSortChange(): Subscription {
    return this.redditService.sortChange$
      .pipe(
        distinctUntilChanged()
      )
      .subscribe(
        (sort: string) => {
          this.activeSort = sort;
          this.getListData();
        }
      );
  }

  private fetchData(): void {
    this.reddits = this.activatedRoute.snapshot.data['reddits'];
  }

  private getListData(): void {
    this.isLoading = true;

    this.redditRepository.getList(this.activeSort, this.activeCategory, this.activeQuery)
      .pipe(
        finalize(() => {
          this.isLoading = false;
        })
      )
      .subscribe(
        (reddits: RedditContent[]) => {
          this.reddits = reddits;
        }
      );
  }

  private getMoreListData(): void {
    this.isLoading = true;

    this.redditRepository.getList(this.activeSort, this.activeCategory, this.activeQuery)
      .pipe(
        finalize(() => {
          this.isLoading = false;
          this.cd.detectChanges();
        })
      )
      .subscribe(
        (moreReddits: RedditContent[]) => {
          this.reddits = this.reddits.concat(moreReddits);
        }
      );
  }

}
