import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { ACTIVE_MODE } from '@app/shared/enums/active-mode.enum';
import { RedditService } from './reddit.service';

@Component({
  selector: 'smp-reddit',
  templateUrl: './reddit.component.html',
  styleUrls: ['./reddit.component.scss']
})
export class RedditComponent {

  public ACTIVE_MODE = ACTIVE_MODE;

  constructor(
    public redditService: RedditService,
    private router: Router,
  ) { }

  public backToList(): void {
    this.navigateTo('reddit/list');
  }

  private navigateTo(path: string): void {
    this.router.navigate([path], { replaceUrl: true });
  }
}
