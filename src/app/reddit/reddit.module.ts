import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatToolbarModule } from '@angular/material/toolbar';

import { CategoriesComponent } from './filter/categories/categories.component';
import { CommentComponent } from './details/comment/comment.component';
import { DetailsComponent } from './details/details.component';
import { FilterComponent } from './filter/filter.component';
import { ListComponent } from './list/list.component';
import { RedditComponent } from './reddit.component';
import { RedditRepository } from './reddit.repository';
import { RedditRoutingModule } from './reddit-routing.module';
import { RedditService } from './reddit.service';
import { SearchComponent } from './filter/search/search.component';
import { SortComponent } from './filter/sort/sort.component';
import { UiScrollerModule } from '@app/ui/ui-scroller.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RedditRoutingModule,
    UiScrollerModule,

    // Material imports
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatToolbarModule,
  ],
  declarations: [
    CategoriesComponent,
    CommentComponent,
    DetailsComponent,
    FilterComponent,
    ListComponent,
    RedditComponent,
    SearchComponent,
    SortComponent,
  ],
  providers: [
    RedditRepository,
    RedditService,
  ]
})
export class RedditModule { }
