import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import * as _moment from 'moment';

import { Comment } from '@app/shared/models/comment.model';

const moment = _moment;

@Component({
  selector: 'smp-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CommentComponent {

  @Input()
  public comment: Comment = null;

  constructor() { }

  public trackByFn(index, item: Comment): number {
    return index;
  }

  public timeFrom(time: number): string {
    return moment(time * 1000).fromNow();
  }
}
