import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Comment } from '@app/shared/models/comment.model';
import { RedditRepository } from '@app/reddit/reddit.repository';
import { RedditService } from '@app/reddit/reddit.service';

@Injectable()
export class CommentsResolve implements Resolve<Comment[]> {
  constructor(
    private redditRepository: RedditRepository,
    private redditService: RedditService,
  ) { }

  resolve(): Observable<Comment[]> {
    return this.redditRepository.getComments(
      this.redditService.activeReddit.data.subreddit,
      this.redditService.activeReddit.data.id
      )
    .pipe(
      catchError(
        (data) => {
          console.log(data, 'eeer');
          
          return of(null);
        }
      )
    );
  }

}
