import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ACTIVE_MODE } from '@app/shared/enums/active-mode.enum';
import { Comment } from '@app/shared/models/comment.model';
import { RedditService } from '../reddit.service';

@Component({
  selector: 'smp-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent {

  public ACTIVE_MODE = ACTIVE_MODE;
  public comments: Comment[] = [];
  public isLoading: boolean = false;

  constructor(
    public redditService: RedditService,
    private activatedRoute: ActivatedRoute,
  ) {
    this.fetchData();
    this.redditService.activeMode = ACTIVE_MODE.DETAILS;
  }

  public trackByFn(index, item: Comment): number {
    return index;
  }

  public hasThumbnail(url: string): boolean {
    return url.includes("https") || url.includes("http");
  }

  private fetchData(): void {
    this.comments = this.activatedRoute.snapshot.data['comments'];
  }

}
