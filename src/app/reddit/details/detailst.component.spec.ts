import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatCardModule } from '@angular/material/card';

import { CommentComponent } from './comment/comment.component';
import { DetailsComponent } from './details.component';
import { RedditService } from '../reddit.service';
import { UiScrollerModule } from '@app/ui/ui-scroller.module';

const redditServiceMock = {};

const activatedRouteMock = {
  snapshot: {
    data: {
      comments: []
    }
  }
};

describe('Component: DetailsComponent', () => {
  let component: DetailsComponent;
  let fixture: ComponentFixture<DetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        UiScrollerModule,

        // Material
        MatCardModule,
      ],
      declarations: [
        DetailsComponent,
        CommentComponent,
      ],
      providers: [
        {
          provide: RedditService,
          useValue: redditServiceMock
        },
        {
          provide: ActivatedRoute,
          useValue: activatedRouteMock
        },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsComponent);
    component = fixture.componentInstance;
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it('should return false if string container has not http or https', () => {
    const result = component.hasThumbnail('www.google.com');

    expect(result).toBeFalsy();
  });

});
