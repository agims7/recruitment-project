import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';

import { RedditComponent } from './reddit.component';
import { RedditService } from './reddit.service';
import { MatIconModule } from '@angular/material/icon';
import { RouterTestingModule } from '@angular/router/testing';

const routerMock = {
  navigate: jest.fn()
};

const redditServiceMock = {};

describe('Component: RedditComponent', () => {
  let component: RedditComponent;
  let fixture: ComponentFixture<RedditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MatButtonModule,
        MatToolbarModule,
        MatIconModule
      ],
      declarations: [
        RedditComponent
      ],
      providers: [
        {
          provide: RedditService,
          useValue: redditServiceMock
        },
        {
          provide: Router,
          useValue: routerMock
        },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RedditComponent);
    component = fixture.componentInstance;
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it('should navigate to akd module on navigateToModule', () => {
    component.backToList();
    expect(routerMock.navigate).toHaveBeenCalledWith(['reddit/list'], {replaceUrl: true});
  });

});
